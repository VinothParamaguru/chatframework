# MIT License

# Copyright(c) 2019 VinothParamaguru

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files(the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys
from database import CDataBase
from exception import CPException
from exception import RaiseException
from exception import GetError
from security import CSecurity
from utils import CUtils
import secrets

class CLoginProcessor(object):

    def __init__(self, username, password):

        self.username = username
        self.password = password
        self.db = CDataBase()
        self.db.Connect()
        # think a better way to handle configurations. hardcoding a config property is a bad design
        self.max_incorrect_login_attempts = 3
        
    def __del__(self):
        
        self.db.Close()
        
    def __SetAccountLock(self):
        
        db = self.db
        sql = 'UPDATE CUsers SET account_locked = :account_locked: WHERE username = :username: AND status = :status:'
        db.InitStatement(sql)
        db.BindParam(":username:", self.username, "string")
        db.BindParam(":status:", 1, "string")
        db.BindParam(":account_locked:", 1, "string")
        db.Update()
        
    def __SetIncorrectLoginAttempts(self, attempts):

        db = self.db
        sql = 'UPDATE CUsers SET incorrect_login_attempts = :incorrect_login_attempts: WHERE username = :username: AND status = :status:'
        db.InitStatement(sql)
        db.BindParam(":username:", self.username, "string")
        db.BindParam(":status:", 1, "string")
        db.BindParam(":incorrect_login_attempts:", attempts, "string")
        db.Update()

    def PerformLogin(self):

        try:
        
            if CSecurity.Validate('email', self.username) == False:
                RaiseException(11)

            if CSecurity.Validate('password', self.password) == False:
                RaiseException(11)
                
            db = self.db
            
            user_record = CUtils.GetUserDetails(db, self.username)
            
            if len(user_record) != 1:
                RaiseException(102)
                
            if user_record[0][5] == 1:
                RaiseException(103)

            # this needs a change. need to access password by column name, not using index
            hashed_password = user_record[0][2]
            
            password_match = CUtils.CheckPassword(self.password, hashed_password)
            
            if password_match == False:
                
                login_attempts_made = user_record[0][6]
                
                if login_attempts_made < self.max_incorrect_login_attempts:
                    self.__SetIncorrectLoginAttempts(user_record[0][6] + 1)
                    RaiseException(102)
    
                elif login_attempts_made == self.max_incorrect_login_attempts:
                    self.__SetAccountLock()
                    RaiseException(103)
                    
            user_token = secrets.token_hex(16)
            session_cols = ('User_ID_PK', 'token', 'status')
            session_vals = (user_record[0][0], user_token, 1)
            db.Insert('CSession', session_cols, session_vals)

        except CPException as e:

            print("<p>Error: %s</p>" % GetError(e.args[0]))
  
        except:

            print("Unexpected error:", sys.exc_info()[0])
