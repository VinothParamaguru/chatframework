# MIT License

# Copyright(c) 2019 VinothParamaguru

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files(the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


import sys
from security import CSecurity
from database import CDataBase
from exception import CPException
from exception import RaiseException
from exception import error_list
from exception import GetError
from utils import CUtils

class CRegister(object):

    def __init__(self, username, password, dob, phone):

        self.username = username
        self.password = password
        self.dob = dob
        self.phone = phone
        
        # connect to db
        self.db = CDataBase()
        self.db.Connect()
    
    def __del__(self):
        
        self.db.Close()

    def PerformRegistration(self):

        response = None
        
        try:

            if CSecurity.ValidateRequiredFields([self.username, self.password, self.phone, self.dob]) == False:
                RaiseException(13)

            if CSecurity.Validate('email', self.username) == False:
                RaiseException(11)

            if CSecurity.Validate('password', self.password) == False:
                RaiseException(11)
            
            if CSecurity.Validate('phone', self.phone) == False:
                RaiseException(11)
                
            if CSecurity.Validate('dob', self.dob) == False:
                RaiseException(11)
            
            db = self.db
            
            if len(CUtils.GetUserDetails(db, self.username)) != 0:
                RaiseException(101)
                
            formatted_dob = CUtils.FormatDate(self.dob)
            
            hashed_password = CUtils.GetHashedPassword(self.password)
            
            fields = ('username', 'password', 'dob', 'phone', 
                      'account_locked','incorrect_login_attempts', 'status')
            
            row = (self.username, hashed_password, formatted_dob, self.phone, 0, 0, 1)
            
            db.Insert('CUsers', fields, row)
            
            response = CUtils.GetAckMsg()

        except CPException as e:

            response = CUtils.GetErrorMsg(e.args[0])
            
        except :
        
            response = CUtils.GetErrorMsg(10000)

        return response