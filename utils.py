# MIT License

# Copyright(c) 2019 VinothParamaguru

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files(the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import bcrypt
import json
from exception import GetError
from queue import Queue
from threading import Lock

class CUtils(object):
    
    @staticmethod
    def Split(input_string, separator):
        
        result_list = input_string.split(separator)
        return result_list
    
    @staticmethod
    def FormatDate(date_string):
        
        dob_tokens = CUtils.Split(date_string, '/')
        date_string = dob_tokens[2] + '-' + dob_tokens[1] + '-' + dob_tokens[0]
        return date_string
    
    @staticmethod
    def GetHashedPassword(password):
        
        salt = bcrypt.gensalt()
        hashed_password = bcrypt.hashpw(password.encode(), salt)
        return hashed_password
    
    @staticmethod
    def CheckPassword(password, hashed_password):
        
        if bcrypt.checkpw(password.encode(), hashed_password.encode()) == True:
            return True
        else:
            return False        
        
    @staticmethod
    def GetUserDetails(db, username):

        sql = 'SELECT * FROM CUsers WHERE username = :username: '
        db.InitStatement(sql)
        db.BindParam(":username:", username, "string")
        results = db.Select()
        return results
    
    @staticmethod
    def GetJsonString(dict_values):
        
        json_data = json.dumps(dict_values)
        return json_data
    
    @staticmethod
    def GetAckData():
        
        ack_data = CUtils.GetJsonString({'ack' : 'ack'})
        return ack_data
    
    @staticmethod
    def GetAckMsg():
        
        data = CUtils.GetAckData()
        print(data)
        
    @staticmethod
    def GetErrorMsg(error_code):
        
        error_params = {}
        error_params['status'] = 'error'
        error_params['error_code'] = error_code
        error_params['message'] = GetError(error_code)         
        error_data = CUtils.GetJsonString(error_params)
        return error_data
        
    @staticmethod
    def ParseJson(json_string):
        
        json_object = json.loads(json_string)
        return json_object
    
    @staticmethod 
    def ParseJsonFromUtfStr(utf_json_string): 
        
        decoded_json_string = utf_json_string.decode('utf-8')
        json_object = CUtils.ParseJson(decoded_json_string)
        return json_object

    @staticmethod
    def GetMsgQueueTemplate():

         msg_queue_template = {
             'queue': Queue(),
             'lock': Lock()
         }

         return msg_queue_template

    @staticmethod
    def EnQueue(msg_queue, msg):

        msg_queue['lock'].acquire()
        try:
            msg_queue['queue'].put(msg)
        finally:
            msg_queue['lock'].release()

    @staticmethod
    def DeQueue(msg_queue):
        
        msg = None
        msg_queue['lock'].acquire()
        try:
            msg = msg_queue['queue'].get()
        finally:
            msg_queue['lock'].release()
        return msg

    @staticmethod
    def IsQueueEmpty(msg_queue):
        
        is_empty = None
        msg_queue['lock'].acquire()
        try:
            is_empty = msg_queue['queue'].empty()
        finally:
            msg_queue['lock'].release()
        return is_empty
