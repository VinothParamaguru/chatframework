# MIT License

# Copyright(c) 2019 VinothParamaguru

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files(the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


from threading import Thread, Event, Lock
from time import sleep 
from utils import CUtils
from register import CRegister
from loginprocessor import CLoginProcessor
from messagequeue import msg_queue_read as g_msg_queue_read
from messagequeue import msg_queue_write as g_msg_queue_write

class CMessageProcessor(Thread): 

    def __init__(self, socket):

        Thread.__init__(self)
        self.socket = socket
        self.message_event = Event()
        self.message_event.clear()
        self.exit_requested = False
        self.buffer_size = (1024 * 4) # 4KB buffer

    def run(self):

        while True:

            # wait until the polling says there is a message from the client
            self.message_event.wait()

            # client request event happened

            # client logs off or terminates connection
            if self.exit_requested == True:
                self.exit_requested = False
                self.message_event.clear()
                break

            # process the client 
            
            data_received = CUtils.DeQueue(g_msg_queue_read[self.socket])
            
            request_params = CUtils.ParseJson(data_received)
            
            response = None
            
            if request_params['module_name'] == 'Register' : 
                register_obj = CRegister(request_params['username'], request_params['password'], request_params['phone'], request_params['dob'])
                response = register_obj.PerformRegistration()
                
            elif request_params['module_name'] == 'Login' : 
                login_obj = CLoginProcessor(request_params['username'], request_params['password'])
                response = login_obj.PerformLogin()
            
            if len(response) > 0 :
                print(response)
            
            CUtils.EnQueue(g_msg_queue_write[self.socket], response)

            self.message_event.clear()
            
    def SetEvent(self):

        if self.message_event.is_set() == False:
            self.message_event.set()

    def Exit(self):

        self.exit_requested = True
        self.SetEvent()



        
