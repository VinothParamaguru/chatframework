# MIT License

# Copyright(c) 2019 VinothParamaguru

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files(the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.



import sys
import socket
import select
from queue import Queue
from threading import Lock

from utils import CUtils
from messagequeue import msg_queue_read as g_msg_queue_read
from messagequeue import msg_queue_write as g_msg_queue_write
from messageprocessor import CMessageProcessor

class CTCPServer(object):

    # Constructor
    
    def __init__(self, server_port):

        self.server_port = server_port
        self.server_socket = None
        self.server_name = 'localhost'
        self.sockets_for_reading = []
        self.sockets_for_writing = []
        self.sockets_with_errors = []
        self.maximum_supported_clients = 5000
        self.buffer_size = (1024 * 4) # 4KB
        self.client_dictionary = {}
        
        

    # This function should never fail. Keeps Running and listening for the incoming connections.

    def Run(self):

        try:
            # Create a TCP stream based socket
            self.server_socket = socket.socket(family = socket.AF_INET, type = socket.SOCK_STREAM)

            # Tuple to hold address and port
            server_address = (self.server_name, self.server_port)

            # Bind the port
            self.server_socket.bind(server_address)

            # make a non-blocking socket
            self.server_socket.setblocking(False)
            
            # Listen for incoming connections
            self.server_socket.listen(5)
            print("The TCP server has been started. Looking for incoming connections at the port " + str(self.server_port))
            
            # Start polling
            self.sockets_for_reading.append(self.server_socket)
            
            # If a client tries to connect, accept and create a new socket for communication
            while True:
                
                # I/O multiplexing using select() system call. 
                sockets_with_read_events, sockets_with_write_events, sockets_with_error_events = select.select(self.sockets_for_reading, self.sockets_for_writing, self.sockets_with_errors)

                closed_client_list = []
                
                # attend to all read events 
                for socket_to_process in sockets_with_read_events:

                    # New client trying to connect
                    if socket_to_process is self.server_socket:
                    
                        client_socket, client_address = self.server_socket.accept()
                        socket_descriptor = client_socket.fileno()
                        client_thread = CMessageProcessor(socket_descriptor)
                        client_thread.daemon = False

                        client_info = {
                            'socket_descriptor': socket_descriptor,
                            'client_address' : client_address,
                            'client_thread': client_thread
                        }
                        
                        # create the read/write message queues for this client
                        queue_read = CUtils.GetMsgQueueTemplate()
                        g_msg_queue_read[socket_descriptor] = queue_read
                        queue_write = CUtils.GetMsgQueueTemplate()
                        g_msg_queue_write[socket_descriptor] = queue_write
                        
                        self.client_dictionary[socket_descriptor] = client_info
                        self.client_dictionary[socket_descriptor]['client_thread'].start()
                        
                        self.sockets_for_reading.append(client_socket)
                        self.sockets_for_writing.append(client_socket)
                        self.sockets_with_errors.append(client_socket)


                    # Existing client trying to communicate
                    else:
                        
                        socket_descriptor = socket_to_process.fileno()
                        request_payload = socket_to_process.recv(self.buffer_size)
                        request_decoded = request_payload.decode('utf-8')
                        
                        # client issued close()
                        if len(request_decoded) == 0:
                            
                            # exit thread processing
                            closed_client_list.append(socket_to_process)
                            self.client_dictionary[socket_descriptor]['client_thread'].Exit()
                            self.sockets_for_reading.remove(socket_to_process)
                            self.sockets_for_writing.remove(socket_to_process)
                            self.sockets_with_errors.remove(socket_to_process)
                        
                        else:
                            
                            # enqueue request processing and set the event for the thread
                            CUtils.EnQueue(g_msg_queue_read[socket_descriptor], request_decoded)
                            self.client_dictionary[socket_descriptor]['client_thread'].SetEvent()

                
                # attend to all write events
                for socket_to_process in sockets_with_write_events:
                    
                    if socket_to_process in closed_client_list:
                        sockets_with_write_events.remove(socket_to_process)
                        continue
                                        
                    socket_descriptor = socket_to_process.fileno()
                    
                    # if the queue is empty, just ignore and continue
                    if CUtils.IsQueueEmpty(g_msg_queue_write[socket_descriptor]):
                        continue
                    
                    response_payload = CUtils.DeQueue(g_msg_queue_write[socket_descriptor])
                    response_encoded = response_payload.encode('utf-8')
                    socket_to_process.send(response_encoded)
                    
        except:
            errorMessage = sys.exc_info()[0]
            print("<p>Error: %s</p>" % errorMessage)
    
    
        

        
        
        
        
