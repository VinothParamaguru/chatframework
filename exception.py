# MIT License

# Copyright(c) 2019 VinothParamaguru

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files(the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.



# Platform exception class 

class CPException(BaseException):

    def __init__(self, m):
        self.message = m

    def __str__(self):
        return self.message

# Exception table
    
error_list = []

error_list = [
    
    # generic errors
    {'code': 11, 'desc': 'Invalid Input'},
    {'code': 12, 'desc': 'Invalid Parameter supplied to the function'},
    {'code': 13, 'desc': 'Required field missing'},
    
    # db related errors
    {'code': 51, 'desc': 'No parameters specified while initializing prepated statement'},
    {'code': 52, 'desc': 'Duplicate parameters found when initializing prepared statement'},
    {'code': 53, 'desc': 'Not all the parameters are binded in the prepared statement'},
    {'code': 54, 'desc': 'The bind parameter specified is never initialized'},
    {'code': 55, 'desc': 'An unknown error occured while executing the query'},
    
    # User module related errors
    {'code': 101, 'desc': 'Failed registering user'},
    {'code': 102, 'desc': 'Login failed. Please try again'},
    {'code': 103, 'desc': 'Your account has been locked out'},
    
    {'code': 10000, 'desc': 'Unknown error has occured'}
]


def RaiseException(code):
    raise CPException(code)

def GetError(code):
    for error in error_list:
        if error['code'] == code:
            return error['desc']
    return 'Unknown error occured'
