# MIT License

# Copyright(c) 2019 VinothParamaguru

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files(the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


import mysql.connector 
from exception import RaiseException
import re
import json

class CDataBase(object):

    def __init__(self):

        self.db_handle = None
        self.param_list = []
        self.param_dict = {}
        self.sql = None
        
    def Connect(self):

        # read the settings file 
        with open('global_settings.json') as settings_file:
            settings = json.load(settings_file)
        
        # Set the database handle, this creates a session
        db_hostname = settings['database'][0]['hostname']
        db_name = settings['database'][0]['schema']
        db_username = settings['database'][0]['username']
        db_password = settings['database'][0]['password']
        
        self.db_handle = mysql.connector.connect(
            host=db_hostname, user=db_username, password=db_password, database=db_name)



    def Close(self):

        # close session
        self.db_handle.close()

    

    def InitStatement(self, sql):
        
        self.sql = sql
        
        # hold the list of params to be substituted later using the BindParam
        self.param_list = re.findall(r':\w*:', sql)

        if len(self.param_list) == 0:
            raise RaiseException(51)

        if len(self.param_list) != len(set(self.param_list)):
            raise RaiseException(52)
        
        # set place holders for type and value
        for param in self.param_list:
            param_desc = {
                'type' : '',
                'value' : '' 
            }
            self.param_dict[param] = param_desc


    def BindParam(self, param, value, type):

        if(param in self.param_dict.keys()):
            self.param_dict[param]['type'] = type
            self.param_dict[param]['value'] = value
        else:
            RaiseException(54)
        
    def Select(self):
        
        if len(self.sql) > 0 and len(self.param_dict) > 0 and len(self.param_list) > 0 and len(self.param_dict) == len(self.param_list) :
            values_list = [];
            sql = self.sql
            for param in self.param_list:
                sql = sql.replace(param, "%s")
                values_list.append(self.param_dict[param]['value'])
            cursor = self.db_handle.cursor()
            values_tuple = tuple(values_list)
            cursor.execute(sql, values_tuple)
            records = cursor.fetchall()
            return records
        
        else:
            
            raise RaiseException(55)
        

    def Insert(self, tableName, columns, values):

        # Make sure columns and values match
        if len(columns) != len(values):
            raise RaiseException(12)

        # Prepare statement for the insert sql query
        insert_sql = 'INSERT INTO ' + tableName + ' ('
        for column in columns:
            insert_sql += column + ','
        insert_sql = insert_sql[:-1]
        insert_sql += ') VALUES ('
        count = 0
        while count < len(values):
            insert_sql += '%s,'
            count = count + 1
        insert_sql = insert_sql[:-1]
        insert_sql += ');'

        # Execute 
        cursor = self.db_handle.cursor()
        cursor.execute(insert_sql, values)
        self.db_handle.commit()
        
    def Update(self):

        if len(self.sql) > 0 and len(self.param_dict) > 0 and len(self.param_list) > 0 and len(self.param_dict) == len(self.param_list):
            values_list = []
            sql = self.sql
            for param in self.param_list:
                sql = sql.replace(param, "%s")
                values_list.append(self.param_dict[param]['value'])
            cursor = self.db_handle.cursor()
            values_tuple = tuple(values_list)
            cursor.execute(sql, values_tuple)
            self.db_handle.commit()

        else:

            raise RaiseException(55)
            
       


